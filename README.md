# Webbserver IPv6-only
I have started to remove A record for some of my webbservers and I will write what's not working on the server side when legacy A record and HTTPS ipv4hints is removed.
The server{s} have IPv4 connectivity.

# Wordpress Jetpak
Jetpak isn't working without A record
Wordpress API is working with autoupdates etc.

# Wordpress themes
Some Wordpress themes don't work as expected and I have not been enable to find out why some looks like shit and other is ok.

# hstspreload.org
You can't add a server with HSTS without A record. I tried to add A RR for [ipv6kungen.se](https://ipv6kungen.se) to make the first test to pass and then remove it. It worked but it will be removed from the list now with this error.

_Status: ipv6kungen.se was recently submitted to the preload list.
However, ipv6kungen.se has changed its behaviour since it was submitted, and will not be added to the official preload list unless the errors below are resolved:
We cannot connect to https://ipv6kungen.se using TLS ("Get \"https://ipv6kungen.se\": dial tcp [2a02:2350:5:10e:ef:8c98:9092:db4c]:443: connect: cannot assign requested address")._


